# GildedRoseKata

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/GildedRoseKata`. To experiment with that code, run `bin/console` for an interactive prompt.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

